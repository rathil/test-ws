package main

import (
	"github.com/fasthttp/websocket"
	"github.com/valyala/fasthttp"
	"log"
)

func main() {
	server := fasthttp.Server{
		Name: "Test ws",
		Handler: func(ctx *fasthttp.RequestCtx) {
			switch string(ctx.Path()) {
			case "/":
				ctx.SetBodyString("ws://" + string(ctx.Host()) + "/ws")
			case "/ws":
				ws(ctx)
			default:
				ctx.Error("Unsupported path", fasthttp.StatusNotFound)
			}
		},
	}
	panic(server.ListenAndServe(":8080"))
}

var upgrader = websocket.FastHTTPUpgrader{}

func ws(ctx *fasthttp.RequestCtx) {
	if err := upgrader.Upgrade(ctx, func(ws *websocket.Conn) {
		defer ws.Close()
		for {
			mt, message, err := ws.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				break
			}
			log.Printf("recv: %s", message)
			err = ws.WriteMessage(mt, message)
			if err != nil {
				log.Println("write:", err)
				break
			}
		}
	}); err != nil {
		log.Println(err)
	}
}
