module gitlab.com/rathil/test-ws

go 1.17

require (
	github.com/fasthttp/websocket v1.4.3
	github.com/valyala/fasthttp v1.30.0
)

require (
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/savsgio/gotils v0.0.0-20200608150037-a5f6f5aef16c // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
